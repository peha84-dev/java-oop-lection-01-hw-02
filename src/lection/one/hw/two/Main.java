package lection.one.hw.two;

public class Main {

    public static void main(String[] args) {
        Triangle triangleOne = new Triangle(50, 50, 50);
        Triangle triangleTwo = new Triangle(25, 45, 65);

        System.out.println(triangleOne.getAreaTriangle());
        System.out.println(triangleTwo.getAreaTriangle());
    }
}